# USB on Linux

https://superuser.com/a/668881

Install dmg2img

sudo apt-get install dmg2img
Convert DMG image file to ISO file

dmg2img -v -i /path/to/image_file.dmg -o /path/to/image_file.iso
Copy ISO image to USB

sudo dd if=/path/to/image_file.iso of=/dev/sdb &